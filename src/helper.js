export const searchHouse = (house) => {
    let clase;

    if( house === 'Gryffindor' ) {
        clase = 'col-md-5 photo gryffindor';
    } 
    else if ( house === 'Slytherin'  ) {
        clase = 'col-md-5 photo slytherin';
    }
    else if ( house === 'Ravenclaw'  ) { 
        clase = ' col-md-5 photo ravenclaw';
    }
    else if ( house === 'Hufflepuff'  ) { 
        clase = ' col-md-5 photo hufflepuff';
    }
    else{
        clase =' col-md-5 photo desconocido'
    }
    return clase;
}