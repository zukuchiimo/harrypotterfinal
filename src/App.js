
import './assets/styles/styles.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import titulo from './assets/images/title.png'
import React, { Fragment } from 'react'
import AssetsBotons from './Componentes/AssetsBotons'
import CardsPersentation from './Componentes/CardsPersentation'

function App() {
  return (
    <Fragment>
      <div className="backgrondImage">
        <AssetsBotons/>
          <div className="row justify-content-md-center no-gutters">
            <div className="col-md-12 marginTop">
              <img src={titulo} className="title img-fluid " />
              <h2 className="subTitle">Selecciona tu filtro</h2>
            </div>
           <div className="row no-gutters marginTop">
           <div className="col-md-1"></div>
              <div className="col-md-4">
                <button type="button" className="btn btn-primary btnStaff mr-4">Estudiantes</button>
              </div>
              <div className="col-md-2"></div>
              <div className="col-md-4">
                <button type="button" className="btn btn-primary btnStaff">Staff</button>
              </div>
            </div>
          </div>
          <div className="row no-gutters">
              <div className="col-md-12">
                <CardsPersentation/>
              </div>
          </div>
      </div>
    </Fragment>
  );
}

export default App;
