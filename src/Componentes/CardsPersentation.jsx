import React from 'react'
import * as redux from 'redux';
import '../assets/styles/styles.scss';
import recta from '../assets/images/rectTrans.png'
import recta1 from '../assets/images/black.png'
import getData from '../data/hp-characters.json'
import getDataPost from '../data/ssdb.json'
import { v4 as uuidv4 } from 'uuid';
import { searchHouse } from '../helper.js';
import { useDispatch } from 'react-redux'
import { toggleFav } from '../redux/note.actions'
import axios from 'axios';
class CardsPersentation extends React.Component {
    constructor() {
        super();
    }
    state = {
        persons: []
    }


    toogleFavoriteHandler = () => {

        // dispatch(toggleFav())
        console.log("la,zando")
    }

    render() {
        return (
            <div className="row no-gutters marginTop">
                <div className="container">
                    <div className="row">
                        {getData.map((getDetail, index) => {
                            return <div className="col-md-6 col-6 ">
                                <div className="card mb-1">
                                    <div className="row no-gutters">
                                        <div className={searchHouse(getDetail.house)}>
                                            <img src={getDetail.image}
                                                className="card-img-top  rounded-circle photoDetail "
                                            />
                                        </div>
                                        <div className="col-md-7">
                                            <div className="card-body">
                                                <span className="life">  {getDetail.alive == true ? ('Vivo') : ('Finado')} /
                                                    {getDetail.hogwartsStudent == true ? ('Estudiante') : null}
                                                    {getDetail.hogwartsStaff == true ? ('Staff') : null}
                                                    <div >
                                                        <i className="float-right btnFavorite" onClick={this.toogleFavoriteHandler}>
                                                            <img src={recta} alt="" />
                                                        </i>
                                                    </div>
                                                </span>
                                                <div className="cardReponsive">
                                                    <h5 className="card-title">{getDetail.name}</h5>
                                                    <ul>
                                                        <li>Cumpleaños:   {getDetail.dateOfBirth != "" ? getDetail.dateOfBirth : ('Desconocido')}</li>
                                                        <li>Genero: {getDetail.gender != "" ? getDetail.gender : ('Desconocido')}</li>
                                                        <li>Color de Ojos: {getDetail.eyeColour != "" ? getDetail.eyeColour : ('Desconocido')}</li>
                                                        <li>Color de pelo : {getDetail.hairColour != "" ? getDetail.hairColour : ('Desconocido')}</li>

                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        })}
     
                    </div>
                </div>
            </div>

        )
    }
}

export default CardsPersentation;