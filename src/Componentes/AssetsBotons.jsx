
import React, { useState } from 'react'
import recta from '../assets/images/recta.png'
import users from '../assets/images/users.png'
import '../assets/styles/styles.scss';
import { Button, ModalBody, ModalFooter, ModalHeader, Modal, FormGroup, Label, Input } from 'reactstrap'




class AssetsBotons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: this.props.nombre,
            cumpleanios: this.props.cumpleanios,
            ojos: this.props.ojos,
            pelo: this.props.pelo,
            genero: this.props.genero,
            posicion: this.props.posicion,

        };
    
        this.cambionombre= this.cambionombre.bind(this);
        this.cambicumpleanios= this.cambicumpleanios.bind(this);
        this.cambioojos= this.cambioojos.bind(this);
        this.cambiopelo= this.cambiopelo.bind(this);
        this.cambiogenero= this.cambiogenero.bind(this);
        this.cambioposicion= this.cambioposicion.bind(this);
 
    }
    cambionombre(e) {
        this.setState( {
            nombre: e.target.value
     
        })
      } 
      cambicumpleanios(e) {
        this.setState( {
            cumpleanios: e.target.value
     
        })
      } 
      cambioojos(e) {
        this.setState( {
            ojos: e.target.value
     
        })
      } 
      cambiopelo(e) {
        this.setState( {
            pelo: e.target.value
     
        })
      } 
      cambiogenero(e) {
        this.setState( {
       
            genero: !this.state.genero
     
        })
      } 
      cambioposicion(e) {
        this.setState( {
            posicion: !this.state.posicion
     
        })
      } 
    state = {
        actualizarAbierto: false,
    }
    componentWillReceiveProps(nextProps){
        this.setState({nombre: nextProps.nombre})
    }

    abiriModal = () => {
        this.setState({ actualizarAbierto: !this.state.actualizarAbierto })
    }
    seEnvio = (e) =>{
        e.preventDefault();
        

        fetch('http://localhost:3000/posts', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            nombre:this.state.nombre ,
            cumpleanios:this.state.cumpleanios ,
            ojos:this.state.ojos,
            pelo:this.state.pelo ,
            genero:this.state.genero ,
            posicion:this.state.posicion 
        })
 
    },

    );
   
    document.getElementById('formEnvioo').reset();

    }
    render() {
        return (
            <div>
                <div className="row justify-content-md-left no-gutters float-right">
                    <div className="col-md-auto">
                        <Button type="button" className="btn btn-primary colorAssetes">Favoritos
                             <img className="ml-2" src={recta} />
                        </Button>
                    </div>
                    <div className="col-md-auto ">
                        <button type="button" className="btn  btn-primary  colorAssetes "
                            onClick={this.abiriModal}>Agregar
                            <img className="ml-2" src={users} />
                        </button>
                    </div>
                </div>
                <Modal isOpen={this.state.actualizarAbierto}>
                    <ModalHeader>
                        <div className=" row no-gutters">
                        <Label>Agregando un personaje</Label>
                         <Button type="button"  className="close deleteStyle float-right tache" data-dismiss="modal" aria-label="Close" onClick={this.abiriModal}>
                            <span aria-hidden="true" className="float-right">&times;</span>
                        </Button>
                        </div>
                    </ModalHeader>
                    <form onClick={this.seEnvio} id="formEnvioo">
                    <ModalBody>
                        <FormGroup className="row no-gutters">
                            <div className="col-md-5 mr-1">
                                <Label for="nombre">Nombre: </Label>
                                <Input type="text"    className="bgColor"  id="nombre" name ="nombre" value={this.state.nombre} onChange={this.cambionombre}  />
                            </div>
                            <div className="col-md-5 ">
                                <Label for="cumpleanios">cumpleaños: </Label>
                                <Input type="date"    className="bgColor"  id="cumpleanios" name="cumpleanios" value={this.state.cumpleanios} onChange={this.cambicumpleanios}/>
                            </div>
                        </FormGroup>
                        <FormGroup className="row no-gutters">
                            <div className="col-md-5 mr-1">
                                <Label for="ojos">Color de ojos: </Label>
                                <Input type="text"  className="bgColor"  id="ojos" name = "ojos" value={this.state.ojos} onChange={this.cambioojos}  />
                            </div>
                            <div className="col-md-5">
                                <Label for="pelo">Color de pelo: </Label>
                                <Input type="text"  className="bgColor" id="pelo" name ="pelo" value={this.state.pelo} onChange={this.cambiopelo}   />
                            </div>
                        </FormGroup>
                        <FormGroup className="row no-gutters" >
                                <div className="col-md-6"> Genero:  <br/>
                                 
                                        <Label className="col-md-5" >
                                            <Input type="radio" name="genero" onChange={this.cambiogenero}  /> Mujer
                                        </Label>
                                        <Label className="col-md-5" >
                                            <Input type="radio" name="genero"  /> Hombre
                                        </Label>
                                  
                            </div>
                            <div className="col-md-6"> Posicion: <br/>
                                 
                                 <Label className="col-md-5" >
                                     <Input type="radio" name="posicion" value={this.state.posicion} onChange={this.cambioposicion} /> Estudiante
                                 </Label>
                                 <Label className="col-md-5" >
                                     <Input type="radio" name="posicion" value={this.state.posicion}  /> Staff
                                 </Label>
                            </div>
                        </FormGroup>
                        <FormGroup className="row no-gutters" >
                            <div className="col-md-10">
                                <Input type="file" name="myImage" accept="image/png, image/gif, image/jpeg" />
                            </div>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter className="row no-gutters">
                        <div className="col-12">
                            <Input type="submit" value="Guardar" className="btnsave" />
                        </div>
                    </ModalFooter>
                    </form>
                </Modal>
            </div>
        )
    }
}
export default AssetsBotons;